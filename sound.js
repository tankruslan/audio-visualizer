import { hslToRgb } from "./utils";

const WIDTH = 1500;
const HEIHGT = 730;
const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');
canvas.height = HEIHGT;
canvas.width = WIDTH;
let analyzer;
let bufferLenght;

function handleError(err) {
  console.log('You must give access to your mic in order to proceed');
}

async function getAudio() {
  const stream = await navigator.mediaDevices
    .getUserMedia({ audio: true })
    .catch(handleError);
  const audioCtx = new AudioContext();
  analyzer = audioCtx.createAnalyser();
  const source = audioCtx.createMediaStreamSource(stream);
  source.connect(analyzer);
  analyzer.fftSize = 2 ** 11;
  bufferLenght = analyzer.frequencyBinCount;
  const timeData = new Uint8Array(bufferLenght);
  const frequencyData = new Uint8Array(bufferLenght);
  drawTimeData(timeData);
  drawFrequency(frequencyData);
}

function drawTimeData(timeData) {
  analyzer.getByteTimeDomainData(timeData);
  ctx.clearRect(0, 0, WIDTH, HEIHGT);
  ctx.lineWidth = 2;
  ctx.strokeStyle = '#ffc600';
  ctx.beginPath();
  const sliceWidth = WIDTH / bufferLenght;
  let x = 0;
  timeData.forEach((data, i) => {
    const v = data / 128;
    const y = (v * HEIHGT) / 2;
    if (i === 0) {
      ctx.moveTo(x, y);
    } else {
      ctx.lineTo(x, y);
    }
    x += sliceWidth;
  });
  ctx.stroke();
  requestAnimationFrame(() => drawTimeData(timeData));
}

function drawFrequency(frequencyData) {
  analyzer.getByteFrequencyData(frequencyData);
  const barWitdh = (WIDTH / bufferLenght) * 2.5;
  let x = 0;
  frequencyData.forEach(data => {
    const percent = data / 255;
    const [h, s, l] = [360 / (percent * 360) - 2.5, 1, 0.75];
    const barHeight = (HEIHGT * percent) / 2;
    const [r, g, b] = hslToRgb(h, s, l);
    ctx.fillStyle = `rgb(${r}, ${g}, ${b})`;
    ctx.fillRect(x, HEIHGT - barHeight, barWitdh, barHeight);
    x += barWitdh + 2;
  });
  requestAnimationFrame(() => drawFrequency(frequencyData));
}

getAudio();
